//Compile with kernel32
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>


DWORD ParseDir(char *szDir, char *fargptr, char *chkdir, int recurs);


int streq_(char *a, char *b)
{
  unsigned long la, lb;
  la = strlen(a);
  lb = strlen(b);
  if (la != lb) return 0;
  if (memcmp(a,b,la)==0) return 1; else return 0;
}

int FileTypeIsDir(LPCTSTR szPath)
{
  DWORD dwAttrib = GetFileAttributes(szPath);

  if (dwAttrib == INVALID_FILE_ATTRIBUTES) return -1;
  if (!(dwAttrib & FILE_ATTRIBUTE_DIRECTORY)) return 0;
  return 1;
}

long CompareFiles(char *afn, char *bfn)
{
    if (streq_(afn, bfn)) return -1;
    if (!afn || !bfn) return -2;
    FILE *af, *bf;
    char ba=0, bb=0;
    af = fopen(afn, "rb");
    if (!af) return -3;
    bf = fopen(bfn, "rb");
    if (!bf)
    {
        fclose(af);
        return -4;
    }
    long n = 0;
    while (!feof(af))
    {
        fread(&ba,sizeof(char),1,af);
        fread(&bb,sizeof(char),1,bf);
        if (ba != bb || ((feof(af) && !feof(bf)) || (feof(bf) && !feof(af))))
        {
            fclose(af);
            fclose(bf);
            return n+1;
        }
        
        n++;
    }
    if ((feof(af) && !feof(bf)) || (feof(bf) && !feof(af)))
    {
        fclose(af);
        fclose(bf);
        return n+1;
    }
    fclose(af);
    fclose(bf);
    return 0;
}

DWORD ParseDir(char *szDir, char *fargptr, char *chkdir, int recurs)
//szDir is DIR command, fargptr is Actual Directory, chkdir is other directory
{
  HANDLE hFind = INVALID_HANDLE_VALUE;
  DWORD dwError=0;
  WIN32_FIND_DATA ffd;
  hFind = FindFirstFile(szDir, &ffd);
  char chDir[MAX_PATH *2], nchDir[MAX_PATH *2], nchkdir[MAX_PATH *2];
  
  if (INVALID_HANDLE_VALUE == hFind) 
  {
    //DisplayErrorBox(TEXT("FindFirstFile"));
        fprintf(stderr,"\nError Finding File.\n");
    dwError = GetLastError();
    return dwError;
  } 
  long ic = 0;
  int changed = 0;
  char afn[MAX_PATH] = "\0";
  char bfn[MAX_PATH] = "\0";
  long cret;
  do
  {
        //printf("%s, %s\n",ffd.cFileName, fargptr);
        strcpy(bfn, chkdir);
        strcat(bfn,"\\");
        strcat(bfn, ffd.cFileName);
        switch (FileTypeIsDir(bfn))
        {
            case 0:
              if ((ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
              {
                  printf("D: %s is different type!\n", bfn);
              }
              else
              {
                  //Compare the files
                  strcpy(afn, fargptr);
                  strcat(afn,"\\");
                  strcat(afn, ffd.cFileName);

                  cret = CompareFiles(afn, bfn);
                  if (cret < 0) switch (cret)
                  {
                      case -1:
                        printf("S: %s and %s are the SAME FILE!\n", afn, bfn);
                      break;

                      case -3:
                        fprintf(stderr, "Error: %s cannot be opened!\n", afn);
                      break;

                      case -4:
                        fprintf(stderr, "Error: %s cannot be opened!\n", bfn);
                      break;

                      default:
                        fprintf(stderr, "Error: Unknown Error comparing files %s and %s!\n", afn, bfn);
                      break;
                  }
                  else if (cret > 0)
                  {
                      printf("D: %s and %s differ at offset %ld!\n", afn, bfn, cret-1);
                  }
                  else
                  {
                      printf("E: %s and %s are equal!\n", afn, bfn);
                  }
              }
            break;

            case 1:
              if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
              {
                  printf("D: %s is different type\n");
              }
            break;

            case -1:
              printf("D: %s Does not exist!\n", bfn);
            break;

            default:
              fprintf(stderr, "Error: Unknown Error comparing files!\n");
            break;
        }
        
        
        if (recurs && (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && !streq_(ffd.cFileName,".") && !streq_(ffd.cFileName,"..") && FileTypeIsDir(bfn) == 1)
        {
          strcpy(chDir,fargptr);
          strcat(chDir,"\\");
          strcat(chDir, ffd.cFileName);
          strcpy(nchDir,chDir);
          strcat(nchDir,"\\*");
          strcpy(nchkdir,chkdir);
          strcat(nchkdir,"\\");
          strcat(nchkdir, ffd.cFileName);
          dwError = ParseDir(nchDir,chDir, nchkdir, recurs);
          if (dwError != ERROR_NO_MORE_FILES) 
          {
            fprintf(stderr, "Error recursing in %s!\n",chDir);
          }
        }
  } while (FindNextFile(hFind, &ffd) != 0);
  dwError = GetLastError();
  if (dwError != ERROR_NO_MORE_FILES) 
  {
     fprintf(stderr, "Error finding files!\n");
  }
  
  FindClose(hFind);
  return dwError;
}



int main(int argc, char *argv[])
{
  HANDLE hFind = INVALID_HANDLE_VALUE;
  char szDir[MAX_PATH];
  char *fargptr, *fcod = NULL;
  int recurs = 0, fen = 2;
  DWORD dwError=0;
  
  if((argc < 3 || argc > 4) || ((argc > 3) && (streq_(argv[1],"-r"))==0) || (streq_(argv[1],"/?")))
  {
      printf("\nDHSC CompareDirs 1.0\nCopyright (c) 2019 DHeadshot's Software Creations\nMIT Licence\nTwitter @DHeadshot\n");
      printf("Some code taken from https://github.com/dheadshot/ListToCSV,\n  (c) 2018 DHeadshot's Software Creations, MIT Licence.\n");
      printf("Compares two directories and highlights:\n-\tFiles found only in the first directory\n-\tFiles with the same name in both directories but different contents.\n");
      printf("Warning: does not highlight files only in the second directory!\n");
      printf("Usage: %s [-r] <target directory> <comparitive directory>\n", argv[0]);
      printf("\t-r\tGoes through the directories recursively within the path.  \n");
      printf("\t\tWithout this option, it compares only files in the specified \n\t\tdirectory and not in subdirectories.\n");
      printf("\t\tOnly recurses into directories present in both the paths\n\t\tspecified!\n");
      printf("\t<target directory>\tSpecify the directory to search through.\n");
      printf("\t<comparitive directory>\tSpecify the directory to compare against.\n");
      return (1);
  }
  if (streq_(argv[1],"-r"))
  {
    fargptr = argv[2];
    fen = 3;
    recurs = 1;
  }
  else fargptr = argv[1];
  if (argc>fen) fcod = argv[fen];
  if ((strlen(fargptr) > MAX_PATH - 3) || (strlen(fargptr) > MAX_PATH - 3))
  {
    fprintf(stderr,"\nDirectory path is too long.\n");
    return (2);
  }
  strcpy(szDir, fargptr);
  strcat(szDir, "\\*");
  dwError = ParseDir(szDir, fargptr, fcod, recurs);
  if (dwError != ERROR_NO_MORE_FILES) return dwError;
  return 0;
}
